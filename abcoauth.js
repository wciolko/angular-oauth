/**
 @fileOverview

 @toc

 */

'use strict';
var authServices = angular.module('angular-abcoauth', ['restangular', 'LocalStorageModule']);

authServices.constant('AUTH_EVENTS', {
    loginSuccess: 'auth-login-success',
    loginFailed: 'auth-login-failed',
    logoutSuccess: 'auth-logout-success',
    sessionTimeout: 'auth-session-timeout',
    notAuthenticated: 'auth-not-authenticated'
})
    .config(function ($httpProvider) {
        $httpProvider.interceptors.push([
            '$injector',
            function ($injector) {
                return $injector.get('AuthInterceptor');
            }
        ]);
    });

authServices.factory('session', function ($log, localStorageService) {
// Instantiate data when service
    // is loaded
    this._user = JSON.parse(localStorageService.get('session.user'));
    this._accessToken = localStorageService.get('session.accessToken');
    this._refreshToken = localStorageService.get('session.refreshToken');
    this._tokenType = localStorageService.get('session.tokenType');
    this._tokenScope = localStorageService.get('session.tokenScope');
    this._userData = localStorageService.get('session.userData');

    this.getUser = function () {
        return this._user;
    };

    this.setUser = function (user) {
        this._user = user;
        localStorageService.set('session.user', JSON.stringify(user));
        return this;
    };

    this.getAccessToken = function () {
        return this._accessToken;
    };

    this.getRefreshToken = function () {
        return this._refreshToken;
    };

    this.getUserData = function () {
        return this._userData;
    };

    this.getTokenType = function () {
        return this._tokenType;
    };

    this.getTokenScope = function () {
        return this._tokenScope;
    };

    this.setAccessToken = function (token) {
        this._accessToken = token;
        localStorageService.set('session.accessToken', token);
        return this;
    };

    this.setRefreshToken = function (token) {
        this._refreshToken = token;
        localStorageService.set('session.refreshToken', token);
        return this;
    };

    this.setTokenType = function (tokenType) {
        this._tokenType = tokenType;
        localStorageService.set('session.tokenType', tokenType);
        return this;
    };

    this.setTokenScope = function (tokenScope) {
        this._tokenScope = tokenScope;
        localStorageService.set('session.tokenScope', tokenScope);
        return this;
    };


    this.setUserData = function (userData) {
        this._userData = userData;
        localStorageService.set('session.userData', userData);
        return this;
    };

    /**
     * Destroy session
     */
    this.destroy = function destroy() {
        this.setUser(null);
        this.setAccessToken(null);
        this.setRefreshToken(null);
        this.setTokenType(null);
        this.setTokenScope(null);
        this.setUserData(null);
    };

    return this;
});

authServices.controller('LoginCtrl', function ($scope, $rootScope, $log, $cookieStore, $location, AbcOAuth, AUTH_EVENTS) {
    if (AbcOAuth.isAuthenticated()) {
        $location.path("/");
    }
    $scope.credentials = {
        username: '',
        password: ''
    };
    $scope.login = function (credentials) {
        AbcOAuth.login(credentials).then(function () {
            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
        }, function () {
            $scope.alert = 'Invalid credentials';
            $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
        });
    };
});

authServices.controller('LogoutCtrl', function ($scope, $rootScope, $log, $cookies, $location, AbcOAuth, AUTH_EVENTS) {
    AbcOAuth.signOut();
    $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
    $location.path("/");
});

authServices.factory('AbcOAuth', ['$http', '$rootScope', 'Restangular', '$log', 'APPCONFIG', '$cookieStore', '$window', '$q', 'session',
    function ($http, $rootScope, Restangular, $log, APPCONFIG, $cookieStore, $window, $q, session) {

        //public methods & properties
        var self = {};

        self.login = function (credentials) {
            var dfd = $q.defer();

            // Request a token with username & password
            self.getToken(credentials.username, credentials.password).then(function (response) {
                // Set default headers
                Restangular.setDefaultHeaders({Authorization: 'Bearer ' + session.getAccessToken()});
                // Resolve the promise
                dfd.resolve();

                // If request for token fails, reject the promise
            }, function (error) {
                dfd.reject(error);
            });

            return dfd.promise;
        };

        self.signOut = function () {
            session.destroy();
            return self;
        };

        self.setSessionData = function (data) {
            session.setUser(data.user);
            session.setAccessToken(data.access_token);
            if (data.token_type)
                session.setTokenType(data.token_type);
            if (data.scope)
                session.setTokenScope(data.scope);
            if (data.refresh_token)
                session.setRefreshToken(data.refresh_token);
            if (data.data)
                session.setUserData(data.data);
        };

        self.getToken = function (username, password) {
            // Parametrize params for the token endpoint
            var params = $.param({
                username: username,
                password: password
            });
            // Send the $http request and return the promise
            return $http.post(APPCONFIG.authEndpoint + '/authenticates', params, {
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function (response) {
                self.setSessionData(response.data);
            });
        };

        self.refreshToken = function () {
            return $http.get(APPCONFIG.authEndpoint + '/refreshes/' + session.getRefreshToken() + '/token');
        };

        self.getUserInfo = function () {
            return session.getUser();
        };

        self.getUserData = function () {
            return session.getUserData();
        };

        self.isAuthenticated = function () {
            var isAuthenticated = session.getUser() !== null;
            if (isAuthenticated)
                Restangular.setDefaultHeaders({Authorization: 'Bearer ' + session.getAccessToken()});
            return isAuthenticated;
        };

        //private methods and properties - should ONLY expose methods and properties publicly (via the 'return' object) that are supposed to be used; everything else (helper methods that aren't supposed to be called externally) should be private.

        return self;
    }]);

authServices.factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
    return {
        responseError: function (response) {
            $rootScope.$broadcast({
                401: AUTH_EVENTS.notAuthenticated,
                419: AUTH_EVENTS.sessionTimeout,
                440: AUTH_EVENTS.sessionTimeout
            }[response.status], response);
            return $q.reject(response);
        }
    };
});

authServices.run(function ($rootScope, AUTH_EVENTS, AbcOAuth, Restangular, APPCONFIG) {
    Restangular.setBaseUrl(APPCONFIG.apiEndpoint);
    $rootScope.$on('$routeChangeStart', function (event, next) {
        if (!AbcOAuth.isAuthenticated()) {
            event.preventDefault();
            // user is not logged in
            $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
        }
    });
});